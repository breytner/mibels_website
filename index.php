<!DOCTYPE html>
<html lang="en">
<head>
    <title>Mibels</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Favicon -->
    <link rel="shortcut icon" href="imgs//iconos/favicon.png">
    
    <!-- Bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    
    <!-- Bootstrap  -->
	<link rel="stylesheet" href="css/bootstrap.css">
    
    <!-- Important Owl stylesheet -->
    <link rel="stylesheet" href="owl-carousel/owl.carousel.css">
     
    <!-- Default Theme -->
    <link rel="stylesheet" href="owl-carousel/owl.theme.css">

    <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,700' rel='stylesheet' type='text/css'>

       
<style>
    #owl-demo .item img{
        display: block;
        width: 100%;
        height: 20%;
    }
</style>

<script>    
    
    $(document).ready(function() {
     
      $("#owl-demo").owlCarousel({
     
          navigation : false, // Show next and prev buttons
          slideSpeed : 300,
          paginationSpeed : 400,
          singleItem: true,
          autoPlay: 5000,
          // "singleItem:true" is a shortcut for:
          // items : 1, 
          // itemsDesktop : false,
          // itemsDesktopSmall : false,
          // itemsTablet: false,
          // itemsMobile : false
     
      });
     
    });

</script>
    
</head>
    
<body>
    <?php include "components/nav.php";?>
    <?php include "components/banner.php";?>
    <?php include "components/search.php";?>
    <?php include "components/download.php";?>
    <?php include "components/footer.php";?>
    

    <!-- SCRIPTS -->
    <script src="js/jquery-1.9.1.min.js"></script>    
    <!-- Include js plugin -->
    <script src="owl-carousel/owl.carousel.js"></script>

    
    
</body>
</html>
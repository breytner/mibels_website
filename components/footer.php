<footer class="footer">

<div class="container" style="margin: 0 auto;">
  <div class="row">
    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6">
        <div class="footer-col">
             <div class="footer-col-header" >Principales</div>
              <a href="#" ><span>Inicio<br></span></a>
              <a href="#" ><span>Eventos<br></span></a>
              <a href="#" ><span>Ofertas<br></span></a>
              <a href="#" ><span>Directorio<br></span></a>
              <a href="#" ><span>En la mira<br></span></a>
            
        </div>
    </div>

    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6">
        <div class="footer-col">
            <div class="footer-col-header" >Servicios</div>
              <a href="#" ><span>Registra tu empresa<br></span></a>
              <a href="#" ><span>Planes y servicios<br></span></a>
              <a href="#" ><span>Publicar ofertas<br></span></a>
              <a href="#" ><span>Crear eventos<br></span></a>
        </div>
    </div>

    <div class="clearfix visible-xs"></div>

    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6">
        <div class="footer-col">
            <div class="footer-col-header" >La empresa</div>
              <a href="#" ><span>Quiénes somos<br></span></a>
              <a href="#" ><span>Nuestros servicios<br></span></a>
              <a href="#" ><span>Misión y visión<br></span></a>
              <a href="#" ><span>Valores<br></span></a>
              <a href="#" ><span>Contáctanos<br></span></a>
        </div>
    </div>

      
    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6">
        <div class="footer-col">
            <div class="footer-col-header" >Redes sociales</div>
              <a href="#" ><img src="imgs/botones/boton-facebook.svg" width=15%><span> Facebook<br><br></span></a>
              <a href="#" ><img src="imgs/botones/boton-twitter.svg" width=15%><span> Twitter<br><br></span></a>
              <a href="#" ><img src="imgs/botones/boton-instagram.svg" width=15%><span> Instagram<br><br></span></a>            
        </div>
    </div>
      
    <div class="clearfix visible-xs"></div>

    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6">
        <div class="footer-col">
            <div class="footer-col-header" >Descarga la app</div>
              <a href="#" ><img src="imgs/botones/boton-googleplay.svg" width=80%><br><br></a>
              <a href="#" ><img src="imgs/botones/boton-appstore.svg" width=80%></a>         
        </div>
    </div>

    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6">
        <div class="footer-col">
            <div class="footer-col-header" ><img src="imgs/logos/logo-mibels.png" width=63%></div>
            <a href="#" style="font-weight:bold" ><span>&copy Mibels 2016</span></a>
            <span>Todos los derechos reservados</span><br><br>
            <a href="#" style="font-weight:bold" ><span>Términos y condiciones</span></a>
            <a href="#" style="font-weight:bold" ><span>Políticas de privacidad</span></a><br><br>
            <span>Desarrollado por&nbsp&nbsp</span><a href="#" ><img src="imgs/logos/icrea-logo.svg" width=18%></a>       
        </div>
    </div>  
    </div>
    </div>
</footer>
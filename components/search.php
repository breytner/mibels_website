<!-- BUSQUEDA -->

<div class="container" style="margin: 0 auto;">
  <div class="row">
    <div class="col-md-12">
        <h2 align="center">Busca empresas, servicios,<br> eventos y ofertas en Mibels</h2>
    </div>
    </div>
</div>

<div class="container" style="margin: 0 auto;">
    <div class="row" style="position:static;padding-top:1%">
        <div class="col-lg-7 col-centered col-md-7 col-centered col-sm-7 col-centered col-xs-7 col-centered">
            <div class="input-group">
                <input type="text" class="form-control border-right-transparent" placeholder="Buscar..."/> 
                <span class="input-group-btn">
                    <button class="btn btn-default border-left-transparent" type="button" id="search-btn"><img src="imgs/botones/boton-lupa-busqueda.svg" /></button>
                </span>
            </div>

        <div class="row" style="position:static;padding-top:1%">
            <div class="col-lg-3 col-md-3 col-sm-3 ">
                    <img src="imgs/botones/boton-flecha-selecciona.svg" width=98%>
            </div>

            <div class="form-group" style="color:#999999;font-size:1em;">

                        <div class="col-lg-3 col-md-3 col-sm-3  text-right" >

                                <label class="radio-inline active" >
                                    <input type="radio" name="inlineRadioOptions" id="searchRadio1" value="EmpresasServicios" checked=""> Empresas y servicios
                                </label>

                        </div>

                        <div class="clearfix visible-xs"></div>

                        <div class="col-lg-3 col-md-3 col-sm-3  text-right">

                                <label class="radio-inline">
                                    <input type="radio" name="inlineRadioOptions" id="searchRadio2" value="Eventos"> Eventos
                                </label>

                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-3  text-right">

                                <label class="radio-inline">
                                    <input type="radio" name="inlineRadioOptions" id="SearchRadio3" value="Ofertas"> Ofertas
                                </label>

                        </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- /.row -->
<header role="banner" id="fh5co-header">
    <div class="container">
    <nav class="navbar navbar-default  navbar-fixed-top" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                <a class="navbar-brand" href="#"><img src="imgs/logos/logo-mibels.png"></a>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav navbar-center">
                    <li id="nav_home" class="active"><a href="index.php"><img src="imgs/iconos/mibels-inicio.svg">&nbsp Inicio</a></li>
                    <li id="nav_events"><a href="events.php" data-nav-section="events"><img src="imgs/iconos/mibels-eventos.svg"><span>&nbsp Eventos</span></a></li>
                    <li id="nav_sales"><a href="sales.php" data-nav-section="sales"><img src="imgs/iconos/mibels-ofertas.svg"><span>&nbsp Ofertas</span></a></li>
                    <li id="nav_directory"><a href="directory.php" data-nav-section="directory"><img src="imgs/iconos/mibels-directorio.svg"><span>&nbsp Directorio</span></a></li>
                    <li id="nav_favs"><a href="favs.php" data-nav-section="favs"><img src="imgs/iconos/mibels-enlamira.svg"><span>&nbsp En la mira</span></a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#" data-nav-section="press"  data-toggle="modal" data-target="#modalSesion"><span>Registrarse | Iniciar sesión</span></a></li>
                </ul>
            </div>
        </div>
    </nav>
    </div>
</header>

<!-- Modal iniciar sesion -->
<div id="modalSesion" class="modal fade" role="dialog" style="top:20%;">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content" style="background-color:#d5e8ed;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h3 align="center" class="modal-title"><stong>Inicia sesión en Mibels</stong></h3>
        </div>
      <div class="modal-body">
        <form align="center" autocomplete="off">
             <div class="form-group">
                <input type="text"style="width:100%; border-radius:5px; border-color:#f68c1e;" placeholder="Correo electrónico" class="form-control">
             </div>
             <div class="form-group">
                <input type="password"style="width:100%; border-radius:5px; border-color:#f68c1e;" placeholder="Contraseña" class="form-control">
            </div>
        </form>
          <a href="#" class="btn btn-primary" style="width:100%;">Ingresar</a>
          <br><br>
          <center>
            <br><br>
              <b style="color:#f68c1e" align="center">¿No tienes cuenta?</b>
              <a href="#" data-toggle="modal" data-target="#modalNoCuenta" style="width:100%; text-decoration:none; color:#f68c1e;">Ingresa aquí</a>
          </center>
      </div>
    </div>

  </div>
</div>
    
<!-- Modal crear cuenta-->
<div id="modalNoCuenta" class="modal fade" role="dialog" style="top:20%;">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content" style="background-color:#d5e8ed;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h3 align="center" class="modal-title"><stong>Crear cuenta en Mibels</stong></h3>
        </div>
        <div class="modal-body">
            <form align="center" autocomplete="off">
                <div class="form-group">
                    <input type="text"style="width:100%; border-radius:5px" placeholder="Nombre y Apellido" class="form-control">
                </div>
                <div class="form-group">
                    <input type="email"style="width:100%; border-radius:5px" placeholder="Correo electrónico" class="form-control">
                </div>
            
                <div class="form-group">
                    <input type="password"style="width:100%; border-radius:5px" placeholder="Contraseña" class="form-control">
                </div>
            
                <div class="form-group">
                    <input type="password"style="width:100%; border-radius:5px" placeholder="Repetir contraseña" class="form-control">
                </div>
                <div class="form-group">
                    <label class="radio-inline">
                        <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="Masculino"> Masculino
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="inlineRadioOptions" id="inlineRadio2" value="Femenino"> Femenino
                    </label>
                </div>
                <div class="form-group">
                    <p style="color:#e04e01;font-size:15px">* Por favor confirma los datos antes de seguir</p>
                </div> 
             
                <div class="form-group">
                    <label class="checkbox-inline">
                        <input type="checkbox" id="inlineCheckbox1" value="option1"> Acepto las <a style="text-decoration:none;" href="#"><b style="color: #3394db;">condiciones de uso</b></a>
                    </label>
                </div>
          </form>
          <a href="#" class="btn btn-send" style="width:100%;">Crear cuenta</a>
          <br><br>
      </div>
    </div>

  </div>
</div>
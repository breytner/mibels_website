<!DOCTYPE html>
<html lang="en">
<head>
    <title>Mibels Ofertas</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Favicon -->
    <link rel="shortcut icon" href="imgs//iconos/favicon.png">
    
    <!-- Bootstrap -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    
    
    
    <!-- Bootstrap  -->
	<link rel="stylesheet" href="css/bootstrap.css">
    
    <!-- Calendar -->
    <link rel="stylesheet" href="css/calendar.css">
    <style type="text/css">
		.btn-twitter {
			padding-left: 30px;
			background: rgba(0, 0, 0, 0) url(https://platform.twitter.com/widgets/images/btn.27237bab4db188ca749164efd38861b0.png) -20px 9px no-repeat;
		}
		.btn-twitter:hover {
			background-position:  -21px -16px;
		}
	</style>
    
    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,700' rel='stylesheet' type='text/css'>

    
</head>
    
<body>
    <?php include "components/nav.php";?>

    
    
    <div class="container" style="padding-top:50px;padding-bottom:100px; padding-left:100px;">
        <div class="row">    
            <div class="col-lg-10" >
                <div class="page-header">
                    <div class="form-inline" align=center>
                        <div class="btn-group" >
                            <button class="btn text-left" data-calendar-nav="prev"> < </button>
                            <button class="btn text-center"  data-calendar-nav="today"><span></span></button>
                            <button class="btn text-right"  data-calendar-nav="next"> > </button>
                        </div>
                    </div>
	           </div>
            </div>
        </div>
        
        <div class="row">    
            <div class="col-lg-10" >
                <div id="calendar"></div>
            </div>
        </div>
        
    </div>

        
    <script type="text/javascript" src="js/underscore-min.js"></script>
    <script type="text/javascript" src="js/calendar.js"></script>
    <script type="text/javascript" src="js/calendar_language/es-ES.js"></script>
    <script type="text/javascript" src="js/app.js"> </script>
    <script type="text/javascript" src="js/jstz.js"> </script>


    <script type="text/javascript">
        var calendar = $("#calendar").calendar(
            {
                language: 'es-ES',
                tmpl_path: "tmpls/",
                events_source: function () { return []; }
            });         
    </script>
    
    <?php include "components/footer.php";?>
     
    
    <!-- SCRIPTS -->
    <script src="js/jquery-1.9.1.min.js"></script>    
    <!-- Include js plugin -->
    <script src="owl-carousel/owl.carousel.js"></script>
    
    <!-- Change navbar active element -->
    <script> 
        // Get nav section name based on current file name
        var nav_section = document.location.pathname.split("/").slice(-1).toString().replace(".php", "");;
        // Remove 'active' class from all elements in navbar
        $('li[id*="nav_"]').removeClass('active');
        // Add 'active' class current active section in navbar
        $('#nav_'+nav_section).addClass('active');
    </script>
</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Mibels En la Mira</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Favicon -->
    <link rel="shortcut icon" href="imgs//iconos/favicon.png">
    
    <!-- Bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    
    <!-- Bootstrap  -->
	<link rel="stylesheet" href="css/bootstrap.css">
    
    <!-- Important Owl stylesheet -->
    <link rel="stylesheet" href="owl-carousel/owl.carousel.css">
     
    <!-- Default Theme -->
    <link rel="stylesheet" href="owl-carousel/owl.theme.css">

    <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,700' rel='stylesheet' type='text/css'>

    
</head>
    
<body>
    <?php include "components/nav.php";?>
    <?php include "components/footer.php";?>
     
    
    <!-- SCRIPTS -->
    <script src="js/jquery-1.9.1.min.js"></script>    
    <!-- Include js plugin -->
    <script src="owl-carousel/owl.carousel.js"></script>
    
    <!-- Change navbar active element -->
    <script> 
        // Get nav section name based on current file name
        var nav_section = document.location.pathname.split("/").slice(-1).toString().replace(".php", "");;
        // Remove 'active' class from all elements in navbar
        $('li[id*="nav_"]').removeClass('active');
        // Add 'active' class current active section in navbar
        $('#nav_'+nav_section).addClass('active');
    </script>
</body>
</html>